import { Component } from '@angular/core';
import { Customer, Service } from './app.service';
import { ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import * as $ from 'jquery';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [Service]

})
export class AppComponent implements AfterViewInit {
  customers: Customer[];

  constructor(service: Service) {
    this.customers =  service.getCustomers();
  }

  ngAfterViewInit() {}
    
}
